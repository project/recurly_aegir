<?php

namespace Drupal\recurly_aegir\Exceptions;

/**
 * Exception for remote task creation failures.
 */
class TaskCreationFailedException extends \RuntimeException {
}
