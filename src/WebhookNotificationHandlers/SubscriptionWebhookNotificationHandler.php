<?php

namespace Drupal\recurly_aegir\WebhookNotificationHandlers;

/**
 * Processes Recurly notifications for Aegir.
 */
abstract class SubscriptionWebhookNotificationHandler extends WebhookNotificationHandler {

}
